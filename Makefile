format:
	autoflake --in-place --remove-all-unused-imports --ignore-init-module-imports -r .
	black .
	seed-isort-config
	isort .
	flake8