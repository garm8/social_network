import mysql.connector
from mysql.connector import pooling

from social_network.core.config import settings


class MySQLDatabase:
    _instance = None

    def __new__(cls, host, username, password, database, port, pool_size):
        if cls._instance is None:
            cls._instance = super(MySQLDatabase, cls).__new__(cls)
            cls._instance._initialize(host, username, password, database, port, pool_size)
        return cls._instance

    def _initialize(self, host, username, password, database, port, pool_size):
        self.dbconfig = {
            "host": host,
            "user": username,
            "password": password,
            "database": database,
            "port": port,
            "connection_timeout": 300,
            "autocommit": True,
        }
        self.pool_size = pool_size
        self.cnxpool = self._create_connection_pool()

    def _create_connection_pool(self):
        return pooling.MySQLConnectionPool(pool_name="mypool", pool_size=self.pool_size, **self.dbconfig)

    def close_pool(self):
        self.cnxpool._remove_connections()

    def execute_query(self, query, params=None):
        connection = self.cnxpool.get_connection()
        cursor = connection.cursor(dictionary=True)
        try:
            cursor.execute(query, params)
            result = cursor.fetchall()
            return result
        except mysql.connector.Error as err:
            print("Error:", err)
            return None
        finally:
            cursor.close()
            connection.close()


def get_db():
    db = MySQLDatabase(
        host=settings.DB_HOST,
        username=settings.DB_USER,
        password=settings.DB_PASSWORD,
        database=settings.DB_NAME,
        port=settings.DB_PORT,
        pool_size=settings.DB_POOL_SIZE,
    )
    return db
