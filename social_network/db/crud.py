import uuid

from social_network.core.security import generate_hash
from social_network.models.schemas_in import UserIn


class UserCRUD:
    @staticmethod
    def get_user_by_id(user_id: uuid.UUID, db):
        query = f"""
            select id, first_name, second_name, birthdate, sex, city, interests, password from users 
            where id = '{user_id}'
        """
        result = db.execute_query(query)
        if not result:
            return None
        return result[0]

    @staticmethod
    def create_user(user_data: UserIn, db) -> str:
        password_hash = generate_hash(password=user_data.password)

        query = """
            insert into users (id, first_name, second_name, birthdate, sex, city, interests, password) 
            values (%s, %s, %s, %s, %s, %s, %s, %s)
        """
        user_id = str(uuid.uuid4())
        params = (
            user_id,
            user_data.first_name,
            user_data.second_name,
            user_data.birthdate,
            user_data.sex,
            user_data.city,
            user_data.interests,
            password_hash,
        )
        db.execute_query(query, params)

        return user_id
