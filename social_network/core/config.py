import secrets

from pydantic_settings import BaseSettings
from starlette.config import Config

config = Config('.env')


class Settings(BaseSettings):
    DB_HOST: str = config('DB_HOST', cast=str)
    DB_PORT: int = config('DB_PORT', cast=int)
    DB_USER: str = config('DB_USER', cast=str)
    DB_PASSWORD: str = config('DB_PASSWORD', cast=str)
    DB_NAME: str = config('DB_NAME', cast=str)
    DB_POOL_SIZE: int = config('DB_POOL_SIZE', cast=int, default=5)

    ACCESS_TOKEN_EXPIRE_MINUTES: int = config('ACCESS_TOKEN_EXPIRE_MINUTES', cast=int, default=60 * 24 * 8)
    SECRET_KEY: str = secrets.token_urlsafe(32)
    ALGORITHM: str = "HS256"


settings = Settings()
