import hashlib
import os
import typing as t
from datetime import datetime, timedelta

from jose import jwt

from social_network.core.config import settings


def generate_hash(password: str) -> bytes:
    salt = os.urandom(32)  # length 32 bytes
    key = hashlib.pbkdf2_hmac(
        hash_name='sha256', password=password.encode('utf-8'), salt=salt, iterations=1000000
    )  # length 32 bytes
    storage_hash = salt + key  # length 64 bytes
    return storage_hash


def check_password(password: str, storage_hash: bytes) -> bool:
    salt, key = storage_hash[:32], storage_hash[32:]
    new_key = hashlib.pbkdf2_hmac(hash_name='sha256', password=password.encode('utf-8'), salt=salt, iterations=1000000)
    if key == new_key:
        return True
    else:
        return False


def create_access_token(subject: t.Union[str, t.Any], expires_delta: timedelta = None) -> str:
    if expires_delta is not None:
        expires_delta = datetime.utcnow() + expires_delta
    else:
        expires_delta = datetime.utcnow() + timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)

    to_encode = {"exp": expires_delta, "sub": str(subject)}
    encoded_jwt = jwt.encode(to_encode, settings.SECRET_KEY, settings.ALGORITHM)

    return encoded_jwt
