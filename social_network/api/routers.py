from fastapi import APIRouter

user_router = APIRouter(
    prefix='/user',
    tags=['users'],
    responses={404: {'description': 'Not found'}},
)

login_router = APIRouter(
    prefix='/login',
    tags=['login'],
    responses={404: {'description': 'Not found'}},
)
