import typing as t

from fastapi import Depends, HTTPException

from social_network.api.deps import get_current_user
from social_network.api.routers import login_router
from social_network.core.security import check_password, create_access_token
from social_network.db.base import get_db
from social_network.db.crud import UserCRUD
from social_network.models.schemas_in import Login
from social_network.models.schemas_out import Token, UserFullOut


@login_router.post("/", response_model=Token)
def login(login_data: Login, db=Depends(get_db)) -> t.Any:
    user = UserCRUD.get_user_by_id(user_id=login_data.id, db=db)
    if not user or user and not check_password(password=login_data.password, storage_hash=user['password']):
        raise HTTPException(status_code=400, detail="Incorrect email or password")

    return {"token": create_access_token(user['id'])}


@login_router.get("/test/", response_model=UserFullOut)
def test(current_user=Depends(get_current_user)) -> t.Any:
    return current_user
