import typing as t
import uuid

from fastapi import Depends, HTTPException

from social_network.api.routers import user_router
from social_network.db.base import get_db
from social_network.db.crud import UserCRUD
from social_network.models.schemas_in import UserIn
from social_network.models.schemas_out import UserFullOut, UserOut


@user_router.post("/register/", response_model=UserOut)
def register(user: UserIn, db=Depends(get_db)) -> t.Any:
    user_id = UserCRUD.create_user(user_data=user, db=db)
    return {"user_id": user_id}


@user_router.get("/get/{user_id}/", response_model=UserFullOut)
def get_user_by_id(user_id: uuid.UUID, db=Depends(get_db)) -> t.Any:
    user = UserCRUD.get_user_by_id(user_id=user_id, db=db)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    return user
