from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import jwt
from pydantic import ValidationError

from social_network.core.config import settings
from social_network.db.base import get_db
from social_network.db.crud import UserCRUD
from social_network.models.schemas import TokenPayload
from social_network.models.schemas_out import UserFullOut

reusable_oauth = OAuth2PasswordBearer(tokenUrl="/login/", scheme_name="JWT")


def get_current_user(token: str = Depends(reusable_oauth), db=Depends(get_db)):
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=settings.ALGORITHM)
        token_data = TokenPayload(**payload)
    except (jwt.JWTError, ValidationError):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Could not validate credentials",
        )

    user = UserCRUD.get_user_by_id(user_id=token_data.sub, db=db)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    user = UserFullOut(**user)
    return user
