CREATE TABLE IF NOT EXISTS users (
    id VARCHAR(36) DEFAULT (UUID()),
    first_name TEXT NOT NULL,
    second_name TEXT NOT NULL,
    birthdate DATE NOT NULL,
    sex VARCHAR(16) NOT NULL,
    city VARCHAR(64) NOT NULL,
    interests TEXT NOT NULL,
    password BLOB NOT NULL,
    PRIMARY KEY(id)
);
