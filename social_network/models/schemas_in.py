import uuid

from pydantic import BaseModel, Field

from social_network.models.schemas import User


class UserIn(User):
    password: str = Field(..., max_length=128)


class Login(BaseModel):
    id: uuid.UUID
    password: str = Field(..., max_length=128)


class Test(BaseModel):
    id: uuid.UUID
