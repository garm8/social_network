import typing as t
import uuid
from datetime import date

from pydantic import BaseModel, Field


class User(BaseModel):
    first_name: str = Field(..., max_length=128)
    second_name: str = Field(..., max_length=128)
    birthdate: date
    sex: str = Field(..., max_length=16)
    city: str = Field(..., max_length=64)
    interests: str = Field(..., max_length=1024)


class TokenPayload(BaseModel):
    sub: t.Optional[uuid.UUID] = None
