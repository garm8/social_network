import uuid

from pydantic import BaseModel

from social_network.models.schemas import User


class Token(BaseModel):
    token: str


class UserOut(BaseModel):
    user_id: uuid.UUID


class UserFullOut(User):
    id: uuid.UUID
