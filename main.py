import os

import uvicorn
from fastapi import FastAPI

from social_network.api.routers import login_router, user_router
from social_network.core.config import settings
from social_network.db.base import MySQLDatabase

app = FastAPI(
    debug=os.getenv('DEBUG', True),
    title='Social Network',
    description='Прототип социальной сети',
    version='0.0.1',
)

app.include_router(user_router)
app.include_router(login_router)


@app.on_event('startup')
def startup():
    MySQLDatabase(
        host=settings.DB_HOST,
        username=settings.DB_USER,
        password=settings.DB_PASSWORD,
        database=settings.DB_NAME,
        port=settings.DB_PORT,
        pool_size=settings.DB_POOL_SIZE,
    )


@app.on_event('shutdown')
def shutdown():
    db = MySQLDatabase(
        host=settings.DB_HOST,
        username=settings.DB_USER,
        password=settings.DB_PASSWORD,
        database=settings.DB_NAME,
        port=settings.DB_PORT,
        pool_size=settings.DB_POOL_SIZE,
    )
    db.close_pool()


if __name__ == '__main__':
    uvicorn.run('main:app', port=8000, host='0.0.0.0', reload=True)
